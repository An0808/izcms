<?php
    include 'function/function.php';
    	if(isset($_GET['pid']) && filter_var($_GET['pid'],FILTER_VALIDATE_INT,array('min_range'=>1))){
            
            $pid=$_GET['pid'];
        }
     $pageJoin=Pages::getDataIdJoin('p.content','page_id',$pid,0,10);
     include 'views/single.php';

    //  add comment 

     if($_SERVER['REQUEST_METHOD']=='POST'){
        //check errors
        $errors=  array();

        // check name
        if(empty($_POST['name'])){
            $errors[]= 'name';
        }
        else{
            $name=$_POST['name'];
        }

        // check email
        if(isset($_POST['email']) && filter_var($_POST['email'],FILTER_VALIDATE_EMAIL)){
            $email =$_POST['email'];
        }
        else{
            $errors[]='email';
        }

        //check comment
        if(empty($_POST['comment'])){
            $errors[]= 'comment';
        }
        else{
            $comment=$_POST['comment'];
        }

        //chech captcha 
        if(isset($_POST['captcha']) && trim($_POST['captcha']) != $_SESSION['capc']['answer']){
            $errors[]= 'wrong';
        }
        
        if(!empty($_POST['hiddin'])){
            exit();
        }
       $page_id = $pid;


       if(empty($errors)){

        //neu ko co loi say ra thi thuc hien chuy van dữ liệu với hàm addCategories bên Module với mảng chuyền vòa là các giá trị 
            if(Pages::addCommentPage(['page_id'=>$page_id,'author'=> $name,'email'=>$email,'comment'=>$comment,'comment_date'=>'now()']))
            {
                $messages =  '<p class="success">The comment was added successfully!</p>';
            }
            else{
                $messages = '<p class="warning">Could not added to the database due to a system erro.</p>';
            }
       }
       else{
          $messages = '<p class="warning">Plaese fill all the required fields</p>';
       } 
    }
    $pageComent = Pages::getDataCommentPage($pid);
    //print_r($pageComent);
    require_once 'views/comment_form.php';

?>