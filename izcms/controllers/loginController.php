<?php
    include 'function/function.php';
    include 'models/user.php'; 
    if($_SERVER['REQUEST_METHOD']=='POST'){
        $error=array();
        if(isset($_POST['email']) && filter_var($_POST['email'],FILTER_VALIDATE_EMAIL)) {
            $email =$_POST['email'];
        }else{
            $error[] ='email';
        }

        if(isset($_POST['password']) && preg_match('/^[\w\.-]{4,20}$/',$_POST['password'])) {
            $password =$_POST['password'];
        }else{
            $error[] ='password';
        }
        if(empty($error)){    
            if($user = Users::getDataUsersCheck($email,$password) != 0){
                print_r($user);
                redirect_to();
            }
            else{
                $message="<p class='erorr'>Email or password not match or your have not activated your account<p/>";
            }
        }
        else{
            $message = "<p class='erorr>The fill all the required field</p>";
        }
    }
    require_once 'views/login.php';
?>