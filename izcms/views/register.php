<div id="content">
<a href="<?php echo $body?>"> Link</a>
    <h2>Register</h2>
    <?php 
        if(isset($messages)){
            echo $messages;
        }
    ?>
    <form action="" method="post">
        <fieldset>
            <legend>Register</legend>
                <div>
                    <label for="First Name">First Name <span class="required">*</span></label> 
                <input type="text" name="first_name" size="20" maxlength="20" value="<?php if(isset($_POST['first_name'])) echo strip_tags($_POST['first_name']) ?>" tabindex='1' />
                <?php if(isset($errors) && in_array('first_name',$errors)){
                    echo '<p style="color:red">first_name is fields</p>';
                } ?>    
                 </div>
                
                <div>
                    <label for="Last Name">Last Name <span class="required">*</span></label> 
                <input type="text" name="last_name" size="20" maxlength="40" value="<?php if(isset($_POST['last_name'])) echo strip_tags($_POST['last_name']) ?>" tabindex='2' />
                <?php if(isset($errors) && in_array('last_name',$errors)){
                    echo '<p style="color:red">last_name is fields</p>';
                } ?>     
                </div>
                
                <div>
                    <label for="email">Email <span class="required">*</span></label> 
                <input type="text" name="email" size="20" maxlength="80" value="<?php if(isset($_POST['email'])) echo strip_tags($_POST['email']) ?>" tabindex='3' />
                <?php if(isset($errors) && in_array('email',$errors)){
                    echo '<p style="color:red">email is fields</p>';
                } ?> 
                </div>
                
                <div>
                    <label for="password">Password <span class="required">*</span></label> 
                <input type="password" name="password1" size="20" maxlength="20" value="" tabindex='4' />
                <?php if(isset($errors) && in_array('password1',$errors)){
                    echo '<p style="color:red">Password is fields</p>';
                } ?> 
                </div>
                
                <div>
                    <label for="email">Confirm Password <span class="required">*</span> </label> 
                <input type="password" name="password2" size="20" maxlength="20" value="" tabindex='5' />
                <?php if(isset($errors) && in_array('password not match',$errors)){
                    echo '<p style="color:red">password not match</p>';
                } ?>    
            </div>
        </fieldset>
        <p><input type="submit" name="submit" value="Register" /></p>
    </form>
</div>