<!DOCTYPE html>
<html>

<head>
	<meta charset='UTF-8' />
	
	<title></title>
	
	<link rel='stylesheet' href='../css/style.css' />
</head>

<body>
	<?php   include '../includes/header.php' ;	?>
	<?php	include '../includes/siderbar-a-admin.php' ;?>	
    <?php
        require_once '../config/Database.php';
        if(isset($_GET['controller'])){
            $controller= $_GET['controller'];
            // echo $controller;
        }
        else{
            include 'views/indexView.php';
        }
        switch ($controller) {
            case 'categories':
                require_once 'controllers/categoriesController.php';
                break;
            case 'users':
                require_once 'controllers/userController.php';
                break;
            case 'comments':
                require_once 'controllers/commentController.php';
                break;
            case 'pages':
                require_once 'controllers/pageController.php';
            break;
            case 'avtivate':
                require_once 'controllers/avtivateController.php';
            break;
        }
    ?>
	<?php   include '../includes/siderbar-b.php';?>
	<?php   include '../includes/footer.php';?>

</body>
</html>