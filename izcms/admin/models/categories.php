<?php
    require_once '../config/Database.php';
    class Categories extends  Database {
        // get data tabale Categories
        public function allCategories(){
            return parent::getAllData('categories','position');
        }
        
        // lấy data từ 2 bảng 
        public function allCategorriesJoin($order_by){
            $result = self::execute(" SELECT c.car_id, c.cat_name, c.position, CONCAT_WS(' ',first_name,last_name) AS name FROM categories as c JOIN users AS u USING (user_id) ORDER BY $order_by ASC ");
            if(mysqli_num_rows($result) >0){
                while($row=mysqli_fetch_object($result)){
                    $data[]=$row;
                }
            }
            else{
                $data=array();
            }
            return $data;
        }

        // insert data table categories
        public function addCategories($mang){
            return parent::addData('categories',$mang);
        }

        //  tạo dữ liêu lấy ra cho position
        public function countCategories(){
            return parent::count('categories','car_id');
        }

        // get data theo id 

        public function addCategoriesId($id){
            return parent::getDataId('categories','car_id',$id);
        }

        // Update table Categories

        public function updateCategories($mang,$id){
            return parent::updateData('categories',$mang,'car_id',$id);
        }

        //Delete data

        public function deleteCategory($id){
            return parent::deleteData('categories','car_id',$id);
        }
    }
?>