<?php
    require_once 'models/categories.php';
    require_once '../function/function.php';
    // $a = new categories;
    // print_r($a->allCategories());
    if(isset($_GET['action'])){
        $action=$_GET['action'];
    }
    else{
        $action = null;
    }

    switch($action){

        // view category
        case 'view':
            $categories = categories::allCategories();

            // sort 
            if(isset($_GET['sort'])){
                $sort=$_GET['sort'];
            }
            else{
                $sort = null;
            }
            switch($sort){
                case 'cat':
                    $order_by = 'cat_name';
                    break;
                case 'pos':
                    $order_by = 'position';
                    break;
                case 'by':
                    $order_by ='name';
                    break;
                    
                default:
                    $order_by='position';
                    break;


            }
            $categoriesJoin=Categories::allCategorriesJoin($order_by);
            require_once 'views/viewsCategories.php';
            break;

            // add  category
        case 'add':
            $num =categories::countCategories();
            // echo $num.'fffffffffffffff';
            $categories = categories::allCategories();
            if($_SERVER['REQUEST_METHOD']=='POST'){
                //check errors
                $errors=  array();
                if(empty($_POST['category'])){
                    $errors[]= 'category';
                }
                else{
                    $cat_name=$_POST['category'];
                }
                if(isset($_POST['position']) && filter_var($_POST['position'],FILTER_VALIDATE_INT,array('min_renge'=>1))){
                    $position =$_POST['position'];
                }
                else{
                    $errors[]='position';
                }
               $user_id = 1;
               if(empty($errors)){

                //neu ko co loi say ra thi thuc hien chuy van dữ liệu với hàm addCategories bên Module với mảng chuyền vòa là các giá trị 
                    if(Categories::addCategories(['user_id'=>$user_id,'cat_name'=> $cat_name,'position'=>$position]))
                    {
                        $messages =  '<p>The category was added successfully!</p>';
                    }
                    else{
                        $messages = '<p>Could not added to the database due to a system erro.</p>';
                    }
               }
               else{
                  $messages = '<p>Plaese fill all the required fields</p>';
               } 
            }
            require_once 'views/add_categories.php';
            break;

        case 'edit':
            $num =categories::countCategories();
            if(isset($_GET['id']) &&  filter_var($_GET['id'],FILTER_VALIDATE_INT,array('min_renge'=>1)) ){
              
                $id=$_GET['id'];
                //echo $id;
                if($_SERVER['REQUEST_METHOD']=='POST'){
                //check errors
                $errors=  array();
                if(empty($_POST['category'])){
                    $errors[]= 'category';
                }
                else{
                    $cat_name=$_POST['category'];
                }
                if(isset($_POST['position']) && filter_var($_POST['position'],FILTER_VALIDATE_INT,array('min_renge'=>1))){
                    $position =$_POST['position'];
                }
                else{
                    $errors[]='position';
                }
               $user_id = 1;
               if(empty($errors)){

                //neu ko co loi say ra thi thuc hien chuy van dữ liệu với hàm updateCategories bên Module với mảng chuyền vòa là các giá trị 
                    if(Categories::updateCategories(['user_id'=>$user_id,'cat_name'=> $cat_name,'position'=>$position],$id))
                    {
                        $messages =  '<p class="success">The category was Update successfully!</p>';
                    }
                    else{
                        $messages = '<p class="warning">Could not Update to the database due to a system erro.</p>';
                    }
               }
               else{
                  $messages = '<p class="warning">Plaese fill all the required fields</p>';
               } 
            }
            }
            else{
                redirect_to_admin();
                // header('Location:index.php');
                // exit();
            }

            $categories = Categories::addCategoriesId($id);
            require_once 'views/edit_categories.php';
            break;

        case 'delete':
            if(isset($_GET['id']) &&  filter_var($_GET['id'],FILTER_VALIDATE_INT,array('min_renge'=>1)) ){
                $id = $_GET['id'];
                if(Categories::deleteCategory($id))
                    {
                        $messages='Đã xoá thành công!';
                        header('Location:index.php?controller=categories&action=view');
                    }
                    else {
                        $messages='Lỗi';
                    }
          
            break;  
            }
    }
    
?>