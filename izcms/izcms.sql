-- phpMyAdmin SQL Dump
-- version 4.9.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 06, 2020 at 12:00 PM
-- Server version: 10.1.43-MariaDB-0ubuntu0.18.04.1
-- PHP Version: 7.1.33-3+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `izcms`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `car_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `cat_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `position` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`car_id`, `user_id`, `cat_name`, `position`) VALUES
(17, 1, 'History', 1),
(18, 1, 'About', 3),
(19, 1, 'HTML', 2),
(20, 1, 'Contact Us', 4),
(21, 1, 'Forum', 5);

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `comment_id` int(10) NOT NULL,
  `page_id` int(10) UNSIGNED NOT NULL,
  `author` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `comment_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`comment_id`, `page_id`, `author`, `email`, `comment`, `comment_date`) VALUES
(1, 5, 'Nguyen Văn An ', 'annguyen323010@gmail.com', 'Bài Viết Hay Qua', '2020-02-04 14:01:32'),
(2, 5, 'Lò Văn Phương ', 'annguyen93449@gmail.com', 'Khá Quá Nhề , Ok Man', '2020-02-04 14:04:32'),
(4, 7, 'Minh Mân ', 'annguyen323010@gmail.com', 'Ok bài cua bạn rất hay và hữu ích ', '2020-02-04 15:09:23'),
(5, 10, 'Mr. Nav ', 'annguyen93449@gmail.com', 'Cảm ơn Nav', '2020-02-04 15:11:25'),
(6, 10, 'Name ', 'annguyen323010@gmail.com', 'aaaaaaaaaaaaaaaaaaaaaaaa', '2020-02-04 15:40:55');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `page_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `car_id` int(10) UNSIGNED NOT NULL,
  `page_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `position` tinyint(4) NOT NULL,
  `post_on` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`page_id`, `user_id`, `car_id`, `page_name`, `content`, `position`, `post_on`) VALUES
(5, 1, 19, 'DVD PSD6 HTML', 'DVD GOOD THANK YOU VERYMUCH', 1, '2020-02-03 11:54:33'),
(6, 1, 18, 'WordPress', 'WordPress very good', 2, '2020-01-30 17:45:13'),
(7, 1, 21, 'Minh Man', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Ut felis. Praesent', 3, '2020-01-31 09:38:18'),
(8, 1, 19, 'Van An ', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Ut felis. Praesent', 2, '2020-01-31 09:39:07'),
(9, 1, 20, 'Demon Warlock', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Ut felis. Praesent', 2, '2020-01-31 10:10:09'),
(10, 1, 20, 'Mr Nav', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Ut felis. Praesent', 3, '2020-01-31 10:10:23'),
(11, 1, 18, 'PHP & MySQL', 'DVD Ve PHP MySQL', 2, '2020-01-31 10:11:05'),
(12, 1, 20, 'virus corona ', 'Mặc dù tác động của những biến đổi trên virus corona vẫn còn chưa được hiểu rõ nhưng các nhà khoa học cảnh báo rằng có khả năng các biến đổi sẽ thay đổi cách thức hoạt động của virus corona chủng mới.\r\n\r\nVirus vẫn liên tục có sự biến đổi, nhưng hầu hết là “biến đổi đồng nghĩa”, tức là không tạo nên sự thay đổi quá lớn trong cách thức hoạt động của mầm bệnh. Mặt khác, một số loại virus có khả năng “biến đổi không đồng nghĩa”, điều có thể làm thay đổi đặc tính sinh học và cho phép virus thích nghi trong các môi trường khác nhau.\r\n\r\nNghiên cứu của giáo sư Cui chỉ ra 2 biến đổi không đồng nghĩa xảy ra trong chủng virus được tìm thấy trong gia đình nhiễm dịch bệnh.\r\n\r\nGiáo sư Cui và cộng sự nhận định rằng khám phá này dường như thể hiện “sự tiến hóa của virus có thể xảy ra khi lây lan từ người qua người”.', 3, '2020-02-04 10:17:06');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `pass` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `website` varchar(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  `yahoo` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bio` text COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_level` tinyint(4) NOT NULL,
  `actve` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `registration_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `first_name`, `last_name`, `email`, `pass`, `website`, `yahoo`, `bio`, `avatar`, `user_level`, `actve`, `registration_date`) VALUES
(1, 'Demon', 'Worlock', 'annguyen323010@gmail.com', 'demon123', 'www.youtobe.com', 'izwebsite', 'Praesent dapibus, neque id cursus,\r\nPraesent dapibus, neque id cursus\r\nPraesent dapibus, neque id cursus', NULL, 2, NULL, '2020-01-21 15:10:05'),
(14, 'qqqqqq', 'qqqqqqqq', 'annguyen93449@gmail.com', '1234567', NULL, NULL, 'http://izcms.test/index.php?controller=avtivate&x=annguyen93449%40gmail.com&y=f807364c6989df4d8e19166e1cb0fdd0', NULL, 0, NULL, '2020-02-06 10:40:07');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`car_id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`comment_id`),
  ADD KEY `page_id` (`page_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`page_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `car_id` (`car_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `registration_date` (`registration_date`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `car_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `comment_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `page_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`page_id`) REFERENCES `pages` (`page_id`);

--
-- Constraints for table `pages`
--
ALTER TABLE `pages`
  ADD CONSTRAINT `pages_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`),
  ADD CONSTRAINT `pages_ibfk_2` FOREIGN KEY (`car_id`) REFERENCES `categories` (`car_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
