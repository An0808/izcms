	<?php
		require_once 'config/Database.php';
		include 'includes/header.php';	

		require_once 'controllers/categoriesController.php';

		if(isset($_GET['controller'])){
            $controller= $_GET['controller'];
            // echo $controller;
        }
        else{
            include 'views/indexView.php';
        }
        switch ($controller) {
            case 'pages':
                require_once 'controllers/pageContent.php';
                break;
            case 'single':
                require_once 'controllers/pageSingle.php';
                break;
            case 'author':
                require_once 'controllers/authorControler.php';
                break;
            case 'contact':
                require_once 'controllers/contactController.php';
                break;
            case 'register':
                require_once 'controllers/registerControler.php';
                break;
            case 'avtivate':
                require_once 'controllers/avtivateController.php';
            break;
            case 'login':
                require_once 'controllers/loginController.php';
            break;
            default:
            // include 'views/indexView.php';
            break;
        }
	
		include 'includes/siderbar-b.php';
		include 'includes/footer.php';
	?>

</body>
</html>